package marcos.collections;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class Collections {

    public static void main(String[] args) {

        System.out.println("-- List --");
        List list = new ArrayList();

        list.add("Brasil");
        list.add("Mexico");
        list.add("USA");
        list.add("Peru");
        list.add("Colombia");
        list.add("Chile");

        for (Object s : list) {
            System.out.println((String) s);
        }
        System.out.println("List size is: " + list.size());

        System.out.println("I live in: " + list.get(3));

        System.out.println("-- Set --");
        Set<Integer> treeSet = new TreeSet<>();
        treeSet.add(45);
        treeSet.add(2);
        treeSet.add(7);
        treeSet.add(7);
        treeSet.add(7);
        treeSet.add(30);
        treeSet.add(5);

        for (int element : treeSet) {
            System.out.println(element + " ");
        }

        System.out.println("-- First Queue --");
        Queue<Integer> q1 = new ArrayBlockingQueue<Integer>(4);
        q1.add(5);
        q1.add(10);
        q1.add(15);
        q1.add(20);

        for(Integer valueX: q1){
            System.out.println("Value: " + valueX);
        }

        System.out.println("Head of queue is: " + q1.element());

        Integer valueX;
        valueX = q1.remove();
        System.out.println("Removed: " + valueX);
        //Second Queue
        System.out.println("-- Second Queue --");
        Queue<Integer> q2 = new ArrayBlockingQueue<Integer>(3);
        q2.offer(78);
        q2.offer(43);
        q2.offer(27);

        System.out.println("Queue 2 peek: " + q2.peek());

        if(q2.offer(13) == false){
            System.out.println("Failed to add fourth item");
        }
        for(Integer value: q2){
            System.out.println("Value: " + value);
        }
        System.out.println("Queue 2 poll: " + q2.poll());

        System.out.println("-- Map --");
        Map<String, String> animals = new HashMap<>();
        animals.put("Mammal","elephant");
        animals.put("Reptile","crocodile");
        animals.put("Amphibian","frog");
        animals.put("Insect","bee");

        System.out.println("Selecting insect: " + animals.get("Insect"));

        for(Map.Entry pairEntry: animals.entrySet()){
            System.out.println(pairEntry.getKey() + " : " + pairEntry.getValue());
        }

    }


}
